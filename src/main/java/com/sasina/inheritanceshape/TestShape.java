/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.inheritanceshape;

/**
 *
 * @author admin
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape();
        shape.print();
        
        Circle circle1 = new Circle(3);
        circle1.calArea();
        circle1.print();
        
        Circle circle2 = new Circle(4);
        
        Triangle triangle = new Triangle(4,3);
        triangle.print();
        
        Rectangle rectangle = new Rectangle(4,3);
        
        Square square = new Square(2);
        
        System.out.println("--------------------");
        
        Shape[] shapes = {circle1, circle2, triangle, rectangle, square};
        for(int i=0; i<shapes.length; i++){
            shapes[i].print();
        }
    }
}
