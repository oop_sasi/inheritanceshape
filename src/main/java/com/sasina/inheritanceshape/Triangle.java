/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.inheritanceshape;

/**
 *
 * @author admin
 */
public class Triangle extends Shape {
    private double base;
    private double height;
    public Triangle(double base, double height){
        super();
        this.base = base;
        this.height = height;
        System.out.println("Triangle created");
    }
    @Override
    public double calArea(){
        return 0.5 * base * height;
    }
    @Override
    public void print(){
        System.out.println("Triangle: base: " + base + ", " + " height: " + height 
                + ", " + "area = " + calArea());
    }
}
