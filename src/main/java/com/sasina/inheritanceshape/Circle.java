/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.inheritanceshape;

/**
 *
 * @author admin
 */
public class Circle extends Shape {
    private double r;
    private double pi = 22/7;
    public Circle(double r){
        super();
        this.r = r;
        System.out.println("Circle created");
    }
    
    @Override
    public double calArea(){
        return pi * r * r;
    }
    @Override
    public void print(){
        System.out.println("Circle: radius: " + r + ", " + "area = " + calArea());
    }
}
